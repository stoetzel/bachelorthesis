#some little script to plott a two-dimensional function

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
import array

wdir = "Plots/"

max_i = int(input("insert max value"))
for i in range(max_i):
    #read in the data to plot
    print("scatter plot is running")
    filename1 = wdir+str(i+1)+"x"
    filename2 = wdir+str(i+1)+"y"
    points = []#use append to add several rows
    file = open(filename1, 'rb')
    bytes = bytearray(file.read())

    points = array.array('f', bytes)

    file.close()


    print("x-read")
    x = np.array(points)

    #npPoints = npPoints.reshape((int(np.sqrt(npPoints.shape[0])), int(np.sqrt(npPoints.shape[0]))))

    points=[]

    file = open(filename2, 'rb')
    bytes = bytearray(file.read())

    points = array.array('f', bytes)

    file.close()

    print("y-read")
    y = np.array(points)
    #Coordinate axis points
    #x = np.arange(-1,1,2/(len(x)))
    #y = np.arange(-1,1,2/(len(y)))

    #newX = np.array([])
    #newY = np.array([])
    #i=1
    #for j in range(len(x)):
    #    newY = np.append(newY, y)
    #    for i in range(len(y)):
    #        newX = np.append(newX, x[j])

    #print(newX.shape)
    #print(newY.shape)

    #create meshgrid
    #x,y = np.meshgrid(x,y)

    #function to plot
    #f = np.sin(np.sqrt(x**2 + y**2))
    #fig = plt.figure()             #doesnt do anything
    #ax = plt.gca(projection='3d') #dunno, 3d plot enable and something else
    #ax2 = plt.gca(projection='3d')

    #print(x.shape)
    #print(y.shape)

    area = 5

    #plot surface
    #surf = ax.plot_wireframe(x, y, npPoints, rstride=1, cstride=1, color='r')#cmap=cm.coolwarm, linewidth=0, antialiased=False)


    plt.scatter(x, y, s=area, c='r', alpha=0.5)

    #file = open(filename2, 'rb')
    #bytes = bytearray(file.read())

    #points = array.array('f', bytes)

    #file.close()
    #npPoints = np.array(points)
    #npPoints = npPoints.reshape((int(np.sqrt(npPoints.shape[0])), int(np.sqrt(npPoints.shape[0]))))

    #surf2 = ax2.plot_wireframe(x, y, npPoints, rstride=1, cstride=1, color='g')


    #Add color bar which maps values to colors
    #fig.colorbar(surf, shrink=0.5, aspect=5)

    #Show the plot
    #plt.show()
    axes = plt.gca()
    axes.set_xlim([-1.,1.])
    axes.set_ylim([-1.,1.])
    if(i+1<10):
        fname = wdir+"00"+str(i+1)+".png"
    if(100>i+1>=10):
        fname = wdir+"0"+str(i+1)+".png"
    if(i+1>=100):
        frame = wdir+str(i+1)+".png"
    plt.savefig(fname, dpi=400, facecolor='w', edgecolor='w',
            orientation='portrait', papertype=None, format='png',
            transparent=False, bbox_inches=None, pad_inches=0.1,
            frameon=None, metadata=None)
    print(i+1)
    plt.clf()
