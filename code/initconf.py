from os import listdir
from os.path import isfile, join
import time
opath = input("insert dir name")
filename = "config.txt"
path = "data/" + opath
onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
print(onlyfiles)
for f in onlyfiles:
    if("data_mag" in f):
        name = path+f
        print(name)
        points = []#use append to add several rows
        file = open(name, 'r')
        z = []
        counter = 0
        for i in file:
            counter = counter + 1
            if (counter>1 and counter < 9):
                try:
                    z.append(round(float(i),3))
                except ValueError as e:
                    e=1
        file.close()
        file = open(filename, 'w')
        file.write( "Pointdensity(nxn): 4000" + "\n"
                    "test: 0" + "\n"
                    "justIntegrate: 1" + "\n"
                    "plotquality: 0" + "\n"
                    "plotvariance: 0" + "\n"
                    "ploterror: 0" + "\n"
                    "p1: " + str(z[0]) + "\n"
                    "p2: " + str(z[1]) + "\n"
                    "p3: " + str(z[2]) + "\n"
                    "p4: " + str(z[3]) + "\n"
                    "t1: " + str(z[4]) + "\n"
                    "t2: " + str(z[5])
                    )
        file.close()
