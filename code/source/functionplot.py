import matplotlib
import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt

a_11 = 7.74585652
a_12 = -3.37875390
a_21 = -9.73040199
a_22 =  1.17775452
b_1 = 9.93038893
b_2 = -0.551673114
k_1 = 0.1
k_2 = -0.4
m_1 = 1.0
m_2 = 1.0
delta = 0.03
detA = a_11*a_22-a_12*a_21
#print(detA)
x = np.arange(-0.999, 0.999, delta)
y = np.arange(-0.999, 0.999, delta)
X, Y = np.meshgrid(x, y)
W1 = np.tanh( a_22/detA*(np.arctanh(-X)-b_1)-a_12/detA*(np.arctanh(-Y)-b_2))
W2 = np.tanh(-a_21/detA*(np.arctanh(-X)-b_1)+a_11/detA*(np.arctanh(-Y)-b_2))
detJ = detA*(1-W1**2)*(1-W2**2)/((1-X**2)*(1-Y**2))
detJx = 1/detJ
#Z = detA*(1-X**2)*(1-Y**2)*np.cosh(a_22/detA*(np.arctanh(X)-b_1)-a_12/detA*(np.arctanh(Y)-b_2))**2*np.cosh(-a_21/detA*(np.arctanh(X)-b_1)+a_11/detA*\
#(np.arctanh(Y)-b_2))**2*(np.exp((-np.tanh(a_22/detA*np.arctanh(X)-a_12/detA*np.arctanh(Y))**2-np.tanh(-a_21*np.arctanh(X)+a_11/detA*np.arctanh(Y))**2)/10**(-0)))
#Z = detA*(1-X**2)*(1-Y**2)*np.cosh(a_22/detA*(np.arctanh(X)-b_1)-a_12/detA*(np.arctanh(Y)-b_2))**2*np.cosh(-a_21/detA*(np.arctanh(X)-b_1)+a_11/detA*(np.arctanh(Y)-b_2))**2\
#*(np.sin(4*np.tanh(a_22/detA*np.arctanh(X)-a_12/detA*np.arctanh(Y)))+np.cos(4*np.tanh(-a_21*np.arctanh(X)+a_11/detA*np.arctanh(Y))))
#Z = abs(10*np.exp((-m_1*(W1-k_1)**2-m_2*(W2-k_2)**2)*10)*abs(detJ)-1)
#Z = 100*np.exp((-m_1*(W1-k_1)**2-m_2*(W2-k_2)**2)*100)*abs(detJ)
#Z = (abs(detJ)/(0.1+(W1-k_1)**2)*1/((W2-k_2)**2+0.1)-1)
t = 0.25/abs(detJ)

t_min, t_max = 0, np.abs(t).max()

fig, (ax0) = plt.subplots(1, 1)
ax=ax0
c = ax.pcolor(X, Y, t, cmap='viridis', vmin=t_min, vmax=t_max)
ax.set_title('Probability distribution of the $x$-coordinates')
fig.colorbar(c, ax=ax)


#c = ax0.pcolor(t)
#ax0.set_title('default: no edges')

fig.tight_layout()
plt.show()
