!> \brief module to configure all relevant constants

module initialization

  use coordinates
  use output

        implicit none

contains
        !> \brief       reads in the config-file "config.txt"
        !> \details     Gets all necessary data from the config file "config.txt"
        !!              if not already build, it will be created in the same dir
        !!              where IsingExe has been executed with some default values.
        !!              All values have to be real numbers.
        !> \author      Tim Stötzel
        !> \date        21.11.2018
        !!
        !! \param[out]  numb_spins          number of spin particles per axis
        !! \param[out]  startstate          value to determine the initial spin-state
        !!                                  (1=all aligned, any other value=random)
        !! \param[out]  magnetic_H          external magnetic field
        !! \param[out]  numb_iterations     number of iterations per mc-step
        !! \param[out]  numb_measures       number of measurements
        !! \param[out]  coupling_J          coupling constant for the system
        !! \param[out]  cwd                 current working directory
        !! \param[out]  plot_En_vs_Temp     create plot energy vs temperature
        !! \param[out]  plot_cap_vs_Temp    create plot (heat) capacity vs temperature
        !! \param[out]  plot_magnet_vs_Temp create plot magnetization vs temperature
        !! \param[out]  plot_var_vs_iter    create plot Energievarianve vs iteration
        !! \param[out]  plot_binary         create image(s) of the spin-lattice
        !! \param[out]  Temp_range          max Temperature which is reached (temp related plots)
        !! \param[out]  Temp_intervall      width of temperature steps (temp related plots)
        !!
        !! \note        config-file layout:           \n
        !!              spin_number             50.0  \n
        !!              startstate              1.0   \n
        !!              number_of_iterations    1.0   \n
        !!              number_of_measurements  100.0 \n
        !!              coupling_constant       1.0   \n
        !!              magnetic_filed          2.0   \n
        !!              dimension               1.0   \n
        !!              plot_En_vs_Temp         0.0   \n
        !!              plot_cap_vs_Temp        0.0   \n
        !!              plot_magnet_vs_Temp     0.0   \n
        !!              plot_var_vs_Iter        0.0   \n
        !!              plot_binary             0.0   \n
        !!              Temp_range              10.0  \n
        !!              Temp_intervall          1.0   \n
        !!              Temp_start              2.0   \n

        !Pointdensity(nxn): 400
        !test: 0
        !justIntegrate: 0
        !plotquality: 0
        !plotvariance: 0
        !plotrealerror: 0

        subroutine init_config(pointDensity, op_test, op_int, op_plotq, op_plotv, op_ploterr,&
                              params, translation)

          integer                   :: io_error, unitNumber
          integer                   :: n
          real, dimension(:,:), intent(inout)   :: params
          real, dimension(:), intent(inout)     :: translation
          integer, intent(out)       :: pointDensity
          real, dimension(1:15)     :: i
          character(len=220)        :: cwd
          character(len=20)         :: empty
          logical                   :: fileexist
          logical, intent(out)      :: op_int, op_test, op_plotq, op_plotv, op_ploterr

          unitNumber = 20
          call getcwd(cwd)                       !get current working dir
          cwd = trim(adjustl(trim(adjustr(cwd))//"/config.txt"))

          inquire(file=cwd, exist=fileexist) !check if file exists

          if(.NOT. fileexist) then          !create file if not found with default params
            print*, "error, file does not exist!"
            print*, "create configuration file: "
            open(unit=unitNumber, file = cwd, &
                  status='new', action='write', iostat=io_error)
              if (io_error == 0) then
                !write(unitNumber,*) "function:          exp(-(x**2+y**2))"
                write(unitNumber,*) "Pointdensity(nxn): 20"
                write(unitNumber,*) "test: 0"
                write(unitNumber,*) "justIntegrate: 0"
                write(unitNumber,*) "plotquality: 0"
                write(unitNumber,*) "plotvariance: 0"
                write(unitNumber,*) "ploterror: 0"
                write(unitNumber,*) "p1: 1.0"
                write(unitNumber,*) "p2: 0.0"
                write(unitNumber,*) "p3: 0.0"
                write(unitNumber,*) "p4: 1.0"
                write(unitNumber,*) "t1: 0.0"
                write(unitNumber,*) "t2: 0.0"
              else
                print*, "error while creating file!"
              end if
            close(unit=unitNumber)
          else
            print*, "file exists"
          end if

          !read config file and save params in array i, empty is just trash
          open(unit=unitNumber, file = cwd, status='old', action='read', &
                iostat=io_error)
                if (io_error == 0) then
                  do n=1, 12 !<< replace with some getFileLength() method
                    read(unitNumber,*) empty, i(n)
                  end do
                else
                  print*, "Error during READ 'config.txt'"
                  print*, "iostat value: "
                  print*, io_error
                end if
          close(unit=unitNumber)
          pointDensity = i(1)
          op_test = int(i(2))
          op_int = int(i(3))
          op_plotq = int(i(4))
          op_plotv = int(i(5))
          op_ploterr = int(i(6))
          params(1,1) = i(7)
          params(1,2) = i(8)
          params(2,1) = i(9)
          params(2,2) = i(10)
          translation(1) = i(11)
          translation(2) = i(12)

        end subroutine


        subroutine sortArray(xarray)
          real, dimension(:), intent(inout)       :: xarray
          integer                                 :: i, j, k
          real                                    :: temp

          i = 1
          j = size(xarray)-1
          do while(i< size(xarray))
            k = 1
            do while(k <= j .AND. j>1)
              if(xarray(k) > xarray(k+1)) then
                temp = xarray(k)
                xarray(k) = xarray(k+1)
                xarray(k+1) = temp
              end if
              k = k + 1
            end do
            j = j - 1
            i = i + 1
          end do
        end subroutine


        subroutine createAxis(xarray)   !creates a equaly distribution of points in 1d
          real, dimension(:), intent(inout)       :: xarray
          integer                                 :: i

          do i=1, size(xarray)
            xarray(i) = (randomPoint()*2.0-1)
          end do

        end subroutine

        subroutine createAxisOLD(xarray)   !creates a equaly distribution of points in 1d
          real, dimension(:), intent(inout)       :: xarray
          real                                    :: randx, time, t2
          integer                                 :: i, seed

          do i=1, size(xarray)
            call cpu_time(time)
            call cpu_time(t2)
            seed = int(time+t2)
            call random_seed(seed)
            call random_number(randx)
            xarray(i) = randx*2-1
          end do

          call sortArray(xarray)

        end subroutine

        function getSeed(a)
          integer, allocatable, dimension(:)            :: getSeed
          integer                                       :: s, time, time2, t,i
          real :: b
          real, intent(in)  :: a

          call cpu_time(b) !counts the clocks that have passed since start
          print*, a,b
          time = int((a-b)*b/a)
          call random_seed(SIZE = s)

          allocate(getSeed(s))

          call random_seed(GET=getSeed)
          t = time-(time/10)*10 !get the first digit
          do i=1, s
            getSeed(i) =(t+i)**2/i+i*23/120+i*t-10*i+15
          end do
          call random_seed(PUT=getSeed)
        end function

        real function randomPoint()
          integer                                  :: time, length, t, s

          !call random_number(randomPoint)
          randomPoint = rand()
        end function


        subroutine createPoints(coords)             !creates the points on each axis
          !real, dimension(:), intent(inout)               :: x, y
          type(tupel), dimension(:,:), intent(inout)      :: coords
          type(tupel)                                     :: tempTupel
          real, dimension(1:2)                            :: tempArray
          integer                                         :: i,j

          do i=1, size(coords(1,:))
            do j=1, size(coords(:,1))
              tempArray = [randomPoint()*2.0-1.0,randomPoint()*2.0-1.0]
              call tempTupel%setCoords(tempArray)
              coords(i,j) = tempTupel
            end do
            if (i>1) then
              call progress(real(i),real(size(coords(1,:))), &
                            int(floor(100*real(i-1)/real(size(coords(1,:))))))
            end if
          end do

        end subroutine

        subroutine createTrafo(params, translation)
          real, dimension(:,:), intent(inout)       :: params
          real, dimension(:), intent(inout)         :: translation
          real                                      :: t
          integer                                   :: i,j

          do i=1, size(params(1,:))
            translation(i) = randomPoint()*3.-1.5
            do j=1, size(params(:,1))
              params(i,j) = randomPoint()*200.-100.
            end do
          end do

          translation = 0.

        end subroutine

end module initialization
