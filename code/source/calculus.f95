module calculus
  use coordinates
  use LUdecomp
  use output
  use omp_lib
  use ieee_arithmetic
  implicit none

  integer, parameter   :: p = selected_real_kind(12, 20)
  integer, parameter   :: q = selected_real_kind(8, 100)
  real, dimension(2)   :: k = (/0.1,0.3/)
  real, dimension(2)   :: m = (/1.0, 1.0/)
  real, parameter      :: peakparam = 100.
  real, parameter      :: punish = 1.

contains

  !function value in the x-coordinates
  real function f(point)
    real, dimension(:), intent(in)                        :: point
    real                                                  :: mass
    !gauss
    !f = peakparam*exp(-peakparam*(m(1)*(point(1)-k(1))**2+&
    !                              m(2)*(point(2)-k(2))**2))
    !lorentzian
    f = 1/(0.0001+((point(1)-k(1))**2+(point(2)-k(2))**2))
    !no-peak
    !f = 1/(point(1) - 1.001)
  end function

  !testpass for peakparam=1,k=0.7,-0.4,m=1.0,2.0
  function fw(point, invparams, translation)                        !calculates the function values for given coordinates
    real, dimension(:), intent(in)          :: point, translation
    real, dimension(:,:), intent(in)        :: invparams
    real                                    :: fw

    !calculate function values for a given point
    !testfunction:
    !f = 3*x**2+(x-2)*y
    !fw = peakparam*exp(-(m(1)*(real(xvec(invparams, translation, point, 1),p)-k(1))**2+&
    !          m(2)*(real(xvec(invparams, translation, point, 2),p)-k(2))**2)*peakparam)
    !seperated lorentzian
    !fw = 1/((real(xvec(invparams, translation, point, 1),p)-k(1))**2+0.01)*&
    !     1/((real(xvec(invparams, translation, point, 2),p)-k(2))**2+0.01)
    !lorentzian
    fw = 1/(0.0001+((real(xvec(invparams, translation, point, 1),p)-k(1))**2&
                 + (real(xvec(invparams, translation, point, 2),p)-k(2))**2))
    !fw = 1/(real(xvec(invparams, translation, point, 1),p)-1.001)
  end function

  !NOT TESTET YET
  !returns the derivative of detJ wrt the inverse matrix A⁻1
  real function detDerv(i,j,k,point,pinv, translation, jac, detJ)
    integer, intent(in)                     :: i,j,k
    real, dimension(:), intent(in)          :: translation, point
    real, dimension(:,:), intent(in)        :: pinv, jac       !matrix inverse of A
    real, intent(in)                        :: detJ
    integer                                 :: c
    if(k==1) then
      if(pinv(i,j)/=0.) then
        if(i/=j) then
          detDerv = -jac(1,2)*jac(2,1)/pinv(i,j)-2*xvec(pinv, translation, point, i)*atanh(point(j))*detJ
        else
          detDerv = jac(1,1)*jac(2,2)/pinv(i,i)-2*xvec(pinv, translation, point, i)*atanh(point(j))*detJ
        end if
      else
        detDerv = -2*xvec(pinv, translation, point, i)*atanh(point(j))*detJ
      end if
    else
      do c=1,2
        detDerv = xvec(pinv,translation,point,c)*pinv(c,i)
      end do
      detDerv = detJ*2*detDerv
    end if

  end function

  real function detDerivative(i,j,k,point, params, translation)
    real, dimension(:), intent(in)          :: point, translation
    real, dimension(:,:), intent(in)        :: params
    real, dimension(1:2,1:2)                :: invparams, jac, dparams
    real, dimension(1:2)                    :: dtranslation
    integer, intent(in)                     :: i,j,k
    integer                                 :: m,n
    real                                    :: delta, deltaJ, detJ

    invparams = invertParams(params)
    delta = 0.0001
    dparams = params
    dtranslation = translation

    if(k==1) then
      dparams(i,j) = params(i,j) - delta
      !new jacobian
      do m=1, size(params(1,:))
        do n=1, size(params(:,1))
          jac(m,n) = JacobianComponents(m,n,dparams,dtranslation, point)
        end do
      end do
      detJ = jac(1,1)*jac(2,2)-jac(1,2)*jac(2,1)

      dparams(i,j) = params(i,j) + delta
      do m=1, size(params(1,:))
        do n=1, size(params(:,1))
          jac(m,n) = JacobianComponents(m,n,dparams,dtranslation, point)
        end do
      end do
      deltaJ = jac(1,1)*jac(2,2)-jac(1,2)*jac(2,1)
    else
      dtranslation(i) = translation(i) - delta
      !new jacobian
      do m=1, size(params(1,:))
        do n=1, size(params(:,1))
          jac(m,n) = JacobianComponents(m,n,dparams,dtranslation, point)
        end do
      end do
      detJ = jac(1,1)*jac(2,2)-jac(1,2)*jac(2,1)

      dtranslation(i) = translation(i) + delta
      do m=1, size(params(1,:))
        do n=1, size(params(:,1))
          jac(m,n) = JacobianComponents(m,n,dparams,dtranslation, point)
        end do
      end do
      deltaJ = jac(1,1)*jac(2,2)-jac(1,2)*jac(2,1)
    end if

    detDerivative = (deltaJ-detJ)/(2*delta)

  end function

  real function fDerivative(i,j,k,point, invparams, translation)
    real, dimension(:,:), intent(in)                  :: invparams
    real, dimension(:), intent(in)                    :: translation, point
    integer, intent(in)                               :: i,j,k
    real, dimension(1:2,1:2)                          :: dparams
    real, dimension(1:2)                              :: dtranslation
    real                                              :: delta, df,f

    delta = 0.0001
    dparams = invparams
    dtranslation = translation

    if(k==1) then
      dparams(i,j) = invparams(i,j) - delta
      f = fw(point, dparams, translation)
      dparams(i,j) = invparams(i,j) + delta
      df = fw(point, dparams, translation)
    else
      dtranslation(i) = translation(i) - delta
      f = fw(point, invparams, dtranslation)
      dtranslation(i) = translation(i) + delta
      df = fw(point, invparams, dtranslation)
    end if

    fDerivative = (df-f)/(2*delta)

  end function

  !wont be used any further
  real function analyticDerivative(point, params, translation, cn)
    real, dimension(:), intent(in)          :: point, translation
    real, dimension(:,:), intent(in)        :: params
    integer, intent(in)                     :: cn  !derivative wrt the coordnumber cn
    real, dimension(1:2,1:2)                :: invparams
    integer                                 :: i


    invparams = invertParams(params)
    analyticDerivative=0.
    do i=1,2
      analyticDerivative = analyticDerivative+invparams(i,cn)/(1-point(cn)**2)*&
                            xvec(params, translation, point, i)
    end do
    analyticDerivative = analyticDerivative*fw(point, params, translation)*(-10**4)*2

    !for the sin function
    !analyticDerivative = analyticDerivative*(4*fw(point, params, translation)-8*&
    !sin(4*(xvec(params, translation, point, 1))))
  end function

  !seems to work
  function invertParams(params)
    real, dimension(1:2,1:2), intent(in)    :: params
    real, dimension(1:2,1:2)   :: invertParams
    real                                    :: detA

    detA = params(1,1)*params(2,2)-params(1,2)*params(2,1)
    invertParams(1,1) = 1/detA*params(2,2)
    invertParams(2,2) = 1/detA*params(1,1)
    invertParams(1,2) = -1/detA*params(1,2)
    invertParams(2,1) = -1/detA*params(2,1)
  end function

  !test pass
  function xvec(invparams, translation, point, i)
    real, dimension(:,:), intent(in)          :: invparams
    real, dimension(:), intent(in)            :: translation, point
    real(p)                      :: xvec
    integer, intent(in)                       :: i

    xvec = tanh(real(invparams(i,1)*(atanh(point(1))-translation(1))+&
             invparams(i,2)*(atanh(point(2))-translation(2)),p))

  end function

  !test pass
  !returns the ij-component of the Jacobian matrix, at a given point
  real function JacobianComponents(i,j,invparams, translation, point)
    real, dimension(1:2,1:2), intent(in)      :: invparams
    real, dimension(1:2)    , intent(in)      :: point  !these are the omega-coords!
    real, dimension(1:2), intent(in)      :: translation
    integer, intent(in)                   :: i,j

      if (invparams(i,j) == 0) then
        JacobianComponents = 0.0
      else
        JacobianComponents = invparams(i,j)/(1-point(j)**2)*(1-xvec(invparams, translation, point,i)**2)
      end if

  end function

  !returns the jacobian matrix of PHI wrt the x-coordinates
  real function jacCompParams(i,j,params,translation,point)
    real, dimension(:,:), intent(in)                :: params
    real, dimension(:), intent(in)                  :: point, translation
    integer, intent(in)                             :: i,j

    if(params(i,j) == 0) then
      jacCompParams = 0.
    else
      jacCompParams = params(i,j)*(1-(tanh(real(params(i,1)*atanh(point(1))+&
                                                params(i,2)*atanh(point(2))+&
                                                translation(i),p)))**2)/&
                                                (1-point(j)**2)
    end if

  end function

  real function determinantLU(umat, lmat)
    real, dimension(:,:), intent(in)    ::  umat, lmat
    integer                             :: i

    determinantLU = 1.0
    do i=1, size(umat(1,:))
      determinantLU = determinantLU*umat(i,i)*lmat(i,i)
    end do
  end function

  real function gradientComponent(i,j,k, point, invparams, translation, &
                                  detJ, jacobian, f, const)
    real, intent(in)                        :: const
    real, dimension(:), intent(in)              :: point, translation
    real, dimension(:,:), intent(inout)         :: jacobian, invparams
    integer, intent(in)                         :: i,j,k
    real, intent(in)                            :: detJ
    real :: sgnJ
    real, intent(inout)                         :: f

    gradientComponent = 0.0

    if(detJ>=0) then
      sgnJ = 1.
    else
      sgnJ = -1.
    end if

    f = fw(point, invparams, translation)

    gradientComponent = punish*2*(f*abs(detJ)-const)*&
                              (fDerivative(i,j,k,point, invparams, translation)*abs(detJ)+&
                              detDerv(i,j,k,point,invparams, translation, jacobian, detJ)*&
                              f*sgnJ)
  end function

  function mcInt(coords, params, upperbound, lowerbound, translation, variance)
    integer           :: n
    real(q)                               :: mcInt
    type(tupel), dimension(:,:), intent(in)   :: coords
    real, dimension(:,:), intent(in)          :: params
    real, dimension(:), intent(in)          :: upperbound, lowerbound, translation
    real, intent(inout)                     :: variance
    integer                       :: i,j,m,v
    real, dimension(1:2)          :: point
    real                          :: a
    real(q)                       :: tempInt, tempvar
    real, dimension(1:2,1:2)      :: jacParams
    real                          :: detJ, oldmean, oldtemp
    integer                       :: lostpoints

    lostpoints = 0
    tempvar = 0.
    oldtemp = 0.

    a = 1.
    do i=1, size(upperbound)
      a = a*(upperbound(i)-lowerbound(i))
    end do

    mcInt = 0.0
    variance = 0.0
    oldmean = 0.
    n = 1

    do i=1,size(coords(1,:))
      do j=1, size(coords(:,1))
        point = coords(i,j)%getCoords()
        do m=1, 2
          do v=1, 2
            jacParams(m,v) = jacCompParams(m,v,params, translation, point)
          end do
        end do

        detJ = jacParams(1,1)*jacParams(2,2)-jacParams(1,2)*jacParams(2,1)

        !CHECK FORMULAR!!!!!
        tempInt = f(point)/abs(detJ)*4.
        if(ieee_is_normal(tempInt)) then
          oldmean = mcInt

          mcInt = mcInt + (tempInt-mcInt)/n

          variance = variance + (tempInt-oldMean)*(tempInt-mcInt)

          n = n + 1
        end if
      end do

    end do
    mcInt = mcInt
    print*, variance
    variance = (variance/real(n)-mcInt**2)/real(n)
    print*, "variance: ", variance
    print*, "lostpoints: ", lostpoints
  end function

  !calculates the integral over the density matrix with a uniformly distributed pointset
  real function density(coords, params, translation)
    type(tupel), dimension(:,:), intent(in)                 :: coords
    real, dimension(:,:), intent(in)        :: params
    real, dimension(:), intent(in)          :: translation
    integer                                 :: i,j,m,v, n
    real                                    :: detJ, oldD, var
    real, dimension(1:2)                    :: point
    real, dimension(1:2,1:2)                :: jacParams

    density = 0.
    n = 1
    oldD = 0.
    var = 0.

    do i=1,size(coords(1,:))
      do j=1, size(coords(:,1))
        point = coords(i,j)%getCoords()
        do m=1, 2
          do v=1, 2
            jacParams(m,v) = jacCompParams(m,v,params, translation, point)
          end do
        end do

        detJ = jacParams(1,1)*jacParams(2,2)-jacParams(1,2)*jacParams(2,1)
        oldD = density
        if(ieee_is_normal(abs(detJ))) then
          density = density + (abs(detJ)-density)/real(n)
          var = var + (abs(detJ)-oldD)*(abs(detJ)-density)
        end if
        n = n + 1
      end do
    end do
    print*, real(n)
    var = var/real(n)
    print*, "variance: ", sqrt(var)
  end function

  real function mcIntGrad(gradient)
    integer           :: n
    real, dimension(:,:), intent(in)        :: gradient
    integer                       :: i,j
    real                         :: a

    mcIntGrad = 0.0
    n = 1
    do i=1,size(gradient(1,:))
      do j=1, size(gradient(:,1))
        mcIntGrad = mcIntGrad + (gradient(i,j)-mcIntGrad)/real(n)
        n = n + 1
      end do
    end do
    !n = size(gradient(:,1))*size(gradient(1,:))
    mcIntGrad = 4.*mcIntGrad!/n
  end function

  real function mcIntOptimizer(fvalue, invparams, translation, detjarray, const)
    integer           :: n
    !type(tupel), dimension(:,:), intent(in)   :: coords
    real, intent(in)                        :: const
    real, dimension(:,:), intent(in)        :: invparams, detjarray, fvalue
    real, dimension(:), intent(in)          :: translation
    integer                       :: i,j,l,m
    real, dimension(1:2,1:2)      :: jac
    real, dimension(1:2)          :: point
    real                          :: a, detJ


    mcIntOptimizer = 0.0
    n = 1
    do i=1,size(fvalue(1,:))
      do j=1, size(fvalue(:,1))
        !do m=1, size(jac(1,:))
        !  do l=1, size(jac(:,1))
        !    jac(m,l) = JacobianComponents(m,l,invparams, translation, point)
        !  end do
        !end do
        !detJ = jac(1,1)*jac(2,2)-jac(1,2)*jac(2,1)
        detJ = detjarray(i,j)
        !mcIntOptimizer = mcIntOptimizer + punish*(fw(point, invparams, translation)*&
        !abs(detJ)-1)**2
        mcIntOptimizer = mcIntOptimizer + ((fvalue(i,j)*abs(detJ)-const)**2-mcIntOptimizer)/real(n)
        n = n + 1
      end do
    end do
    !  n = size(coords(:,1))*size(coords(1,:))
    mcIntOptimizer = 4.*mcIntOptimizer!/n

  end function

  real function optInt(coords, invparams, translation, const, qval)
    integer           :: n
    real, intent(in)                        :: const
    type(tupel), dimension(:,:), intent(in)   :: coords
    real, dimension(:,:), intent(in)        :: invparams
    real, dimension(:), intent(in)          :: translation
    integer                       :: i,j,l,m
    real, dimension(1:2,1:2)      :: jac
    real, dimension(1:2)          :: point
    real                          :: detJ, f
    real, intent(out)             :: qval

    optint = 0.0
    qval = 0.0

    n=1
    do i=1,size(coords(1,:))
      do j=1, size(coords(:,1))
        point = coords(i,j)%getCoords()
        do m=1, size(jac(1,:))
          do l=1, size(jac(:,1))
            jac(m,l) = JacobianComponents(m,l,invparams, translation, point)
          end do
        end do
        detJ = jac(1,1)*jac(2,2)-jac(1,2)*jac(2,1)
        f = (fw(point, invparams, translation)*abs(detJ)*4.)
        if(ieee_is_normal(f)) then
          optint = optint + ((f-const)**2-optint)/real(n)
          qval = qval + (abs(f-const)-qval)/real(n)
        end if
        n=n+1
      end do
    end do
  end function

  subroutine OptimizerRoutine(optint, int, coords, invparams, translation, const, qval)
    integer           :: n
    real, intent(in)                        :: const
    type(tupel), dimension(:,:), intent(in)   :: coords
    real, dimension(:,:), intent(in)        :: invparams
    real, dimension(:), intent(in)          :: translation
    integer                       :: i,j,l,m
    real, dimension(1:2,1:2)      :: jac
    real, dimension(1:2)          :: point
    real                          :: detJ, f
    real, intent(inout)           :: optint, int, qval

    optint = 0.0
    int = 0.0
    qval = 0.0
    n=1
    do i=1,size(coords(1,:))
      do j=1, size(coords(:,1))
        point = coords(i,j)%getCoords()
        do m=1, size(jac(1,:))
          do l=1, size(jac(:,1))
            jac(m,l) = JacobianComponents(m,l,invparams, translation, point)
          end do
        end do
        detJ = jac(1,1)*jac(2,2)-jac(1,2)*jac(2,1)
        f = (fw(point, invparams, translation)*abs(detJ)*4.)
        if(ieee_is_normal(f)) then
          qval = qval + (abs(f-const)-qval)/real(n)

          optint = optint + ((f-const)**2-optint)/real(n)

          int = int + (abs(f)-int)/real(n)
        end if
        n=n+1
      end do
    end do
  end subroutine

end module calculus
