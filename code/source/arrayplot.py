#some little script to plott a two-dimensional function

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter, NullFormatter, FixedLocator
import numpy as np
import array
import time

int = 29.6267

#read in the data to plot
print("scatter plot is running")
filename1 = "Plots/qualityplot_palladium"#"Plots/varplot"
points = []#use append to add several rows
file = open(filename1, 'r')
#bytes = bytearray(file.read())
#print(bytes.decode('ascii'))
#points = array.array('f', bytes)
z = []
for i in file:
    if(float(i)!=0):
        z.append(round(float(i),5))
file.close()
print("x-read")
y = np.array(z)


points=[]
file=open(filename2, 'r')
z=[]
for i in file:
    print(i)
    if(float(i)!=0):
        z.append(round(float(i),5))
file.close()
yi = np.array(z)
x = np.arange(1,y.size+1,1)
print(x.shape)
print(y.shape)
print(yi.shape)


area = 10

ax = plt.gca()

s=np.arange(1,1000,1)
plt.plot(s,1/np.sqrt(s), '-b', label=r'$\propto \frac{1}{\sqrt{N}}$')

<<<<<<< HEAD
#function to plot
#f = np.sin(np.sqrt(x**2 + y**2))
#fig = plt.figure()             #doesnt do anything
#ax = plt.gca(projection='3d') #dunno, 3d plot enable and something else
#ax2 = plt.gca(projection='3d')

#print(x.shape)
#print(y.shape)

area = 10

#plot surface
#surf = ax.plot_wireframe(x, y, npPoints, rstride=1, cstride=1, color='r')#cmap=cm.coolwarm, linewidth=0, antialiased=False)

plt.xlabel("Cycle number")
plt.ylabel("$q$-factor")

plt.scatter(x, y, s=area, c='black', alpha=0.5)

#file = open(filename2, 'rb')
#bytes = bytearray(file.read())

#points = array.array('f', bytes)
||||||| constructed merge base
#function to plot
#f = np.sin(np.sqrt(x**2 + y**2))
#fig = plt.figure()             #doesnt do anything
#ax = plt.gca(projection='3d') #dunno, 3d plot enable and something else
#ax2 = plt.gca(projection='3d')

#print(x.shape)
#print(y.shape)

area = 5

#plot surface
#surf = ax.plot_wireframe(x, y, npPoints, rstride=1, cstride=1, color='r')#cmap=cm.coolwarm, linewidth=0, antialiased=False)


plt.scatter(x, y, s=area, c='r', alpha=0.5)

#file = open(filename2, 'rb')
#bytes = bytearray(file.read())

#points = array.array('f', bytes)
=======
plt.xlabel("Number of grid points $\sqrt{N}$", fontsize=15)
plt.ylabel(r"$\frac{I_N-I}{I}$", fontsize=15)
>>>>>>> checkout to master

plt.scatter(x, y, s=area, c='black', alpha=0.7, label='uniform sampling')
plt.scatter(x, yi,s=area, c='red', alpha=0.7, label='importance sampling')
ax.set_xscale('linear')
ax.set_yscale('log')

if(np.min(y)<np.min(yi)):
    min = np.min(y)
else:
    min = np.min(yi)

ax.set_ylim([min,10])
leg = ax.legend()

fig = plt.gcf()
fig.set_size_inches(8.5,8)

#Show the plot
plt.show()
