module LUdecomp

  use coordinates

  implicit none

contains

  subroutine decomp(a, l, u, n, NoP, pMatrix)
    real, dimension(:,:), intent(inout)         :: a !matrix to decompose
    real, dimension(:,:), intent(inout)         :: l, u !lower/upper resultmatrix
    integer, intent(in)                         :: n !matrix dimension
    integer, intent(inout)                      :: NoP !number of permutations
    integer                                     :: j,k,m, row1, row2
    integer, dimension(n)                       :: counter
    integer                                     ::ccounter, ones
    real, dimension(n,n), intent(inout)         :: pMatrix
    real, dimension(n,n)                        ::  tempMatrix

    counter = 0
    ccounter = 1
    ones = 0
    row1 = 0
    row2 = 0
    pMatrix = 0
    tempMatrix = 0
    do m =1, n
      tempMatrix(m,m) = 1
    end do

    !initialize l as 0 matrix
    do m=1, n
      do j=1,n
        l(j,m) = 0.0
      end do
    end do

      l(1,2) = 0          !needed to fix this manually, check borders of loops!

    !core algorithm
    do m=1, n
      l(m,m) = 1
      !do while(a(m,m)==0)
      !  call permute(a, n, m, NoP)  !permutes with 1->4->3->2->1
      !end do
      !search for two rows to swap

      if(a(m,m) == 0) then
        do k=m+1,n      !maybe add some break condition
          if(a(k,m) /= 0 ) then
            row1 = m
            row2 = k
                  call permutationmatrix(n, row1, row2, pMatrix)
                  NoP = NoP + 1

                  tempMatrix = matmul(pMatrix, tempMatrix)

                  a = matmul(pMatrix,a)

          end if
        end do
      end if

    !  call permutationmatrix(n, row1, row2, pMatrix)
      !a = pMatrix*a

      do j=m+1, n
        if(a(j,m)== 0 .AND. a(m,m) == 0) then
          print*,"zero division found"
          l(j,m) = 0
        else
          l(j,m) = a(j,m)/a(m,m)        !make sure diagonals are nonzero!
        end if

          a(j,m) = 0  !was this here before?
        do k=m+1, n
          a(j,k) = a(j,k)-a(m,k)*l(j,m)
        end do
      end do
    end do

    u = a
    u = transpose(a)
    l = transpose(l)
    pMatrix = tempMatrix


  end subroutine

  !maybe permute in the other direcrtion
  subroutine permute(m, dim, col, numb_perm)  !works
    real, dimension(dim,dim),intent(inout)                      :: m
    integer, intent(inout)                                      :: numb_perm
    integer, intent(in)                                         :: dim, col
    integer                                                     :: i
    real, dimension(dim)                                        :: temp_row

    temp_row = m(dim,:)
    m(dim,:) = m(dim-1,:)
    do i=1, dim-col-1
      m(dim-i,:) = m(dim-1-i,:)
    end do
    m(col,:) = temp_row
    numb_perm = numb_perm + 1
  end subroutine

  subroutine permutationmatrix(dim, row1, row2, p)
    real, dimension(dim,dim), intent(inout)             :: p
    integer, intent(in)                                 :: dim, row1, row2
    integer                                             :: k

    p = 0
    do k=1, dim
      p(k,k) = 1
    end do

    p(row1,row1) = 0
    p(row2, row1) = 1
    p(row2,row2) = 0
    p(row1,row2) = 1

  end subroutine

end module LUdecomp
