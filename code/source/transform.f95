module transform
  !module that contains transformation to transform the output function
  !own classes
  use coordinates
  use calculus
  implicit none

  !variables
  integer, parameter   :: u = selected_real_kind(12, 20)

  contains
    subroutine trafo(coords, params, translation)
      type(tupel), dimension(:,:), intent(inout)  :: coords !set of coords
      real, dimension(:), intent(in)              :: translation
      real, dimension(:,:), intent(inout)         :: params  !trafomatrix
      real, dimension(1:2)                        :: temp1, temp2
      integer                                     :: i, j !counter


      !g = gmatrix(parameters) !trafomatrix, given by gmatrix function
      !do while(...) !edit for dynamic dimension size
      !transforms the coordinates of the given set
      i=1
      do while(i<=size(coords(1,:)))
        j=1
        do while(j<=size(coords(:,2)))
          temp1 = coords(i,j)%getCoords()
          temp2(1) = tanh(params(1,1)*atanh(temp1(1))+params(1,2)*atanh(temp1(2)) + translation(1))
          temp2(2) = tanh(params(2,1)*atanh(temp1(1))+params(2,2)*atanh(temp1(2)) + translation(2))
          call coords(i,j)%setCoords(temp2)
          j = j + 1
        end do
        i = i + 1
      end do

    end subroutine

    subroutine backtrafo(coords, invparams, translation)
      type(tupel), dimension(:,:), intent(inout)      :: coords
      real, dimension(:,:)                            :: invparams
      real, dimension(:)                              :: translation
      real, dimension(1:2)                            :: tempTupel, temp
      integer                                         :: i,j

      do i=1, size(coords(1,:))
        do j=1, size(coords(:,1))
          tempTupel = coords(i,j)%getCoords()
          temp(1) = tanh(real(invparams(1,1)*(atanh(tempTupel(1))-translation(1))+&
                              invparams(1,2)*(atanh(tempTupel(2))-translation(2)),u))
          temp(2) = tanh(real(invparams(2,1)*(atanh(tempTupel(1))-translation(1))+&
                              invparams(2,2)*(atanh(tempTupel(2))-translation(2)),u))
          call coords(i,j)%setCoords(temp)
        end do
      end do

    end subroutine


end module transform
