module algorithm
  use calculus
  use coordinates
  use LUdecomp
  use transform
  use output
  use initialization

  implicit none

  contains
    !subroutine, to find the minimum of the optimization function McIntOp
    subroutine optimize(wcoords, invparams, translation, stepsize, gradMat, terminate, func1, func2, &
                        const, icheck, qval, opt, gradarray, recalc)
      real, intent(in)                                :: const, icheck
      type(tupel), dimension(:,:), intent(in)         :: wcoords
      real, dimension(:,:), intent(inout)             :: invparams, gradMat, func1, func2
      real, dimension(1:2,1:2)                        :: jac1, jac2
      real, dimension(:), intent(inout)               :: translation
      real, dimension(:), intent(inout)               :: qval
      integer                                         :: i,j,k,v      !loop counters
      logical                                         :: run, fixdetected
      logical, dimension(1:6)                         :: fixfound
      integer                                         :: loopcount, compare
      real, dimension(:), intent(inout)               :: stepsize, gradarray
      logical, intent(inout)                          :: terminate, recalc
      real, intent(inout)                             :: opt

      run = .TRUE.

      loopcount = 0
      fixfound = .FALSE.

      !sample for each coordinate direction
      do k=1, 2
        !loop for the matrix-components
        if(k==1) then
          do i=1, 2
            do j=1, 2
              if(i==1) then
                if(gradarray(j) == 0.) recalc=.TRUE.
              else
                if(gradarray(j+2) == 0.) recalc = .TRUE.
              end if
              fixdetected = .TRUE.
              loopcount = loopcount + 1
              call linmin(i,j,k,invparams, translation, wcoords, run, fixfound, stepsize, gradMat, opt, &
                          jac1, jac2, func1, func2, const, icheck, qval, gradarray, recalc)
              if(run) call printParams(invparams, translation)
            end do
          end do
      else  !loop for the translation component(just i is counting)
        do i=1, 2
          if(gradarray(i) == 0.) recalc = .TRUE.
          j=1
          fixdetected = .TRUE.
          call linmin(i,j,k,invparams, translation, wcoords, run, fixfound, stepsize, gradMat, opt, &
                      jac1, jac2, func1, func2, const, icheck, qval, gradarray, recalc)
          if(run) call printParams(invparams, translation)

          loopcount = loopcount + 1

        end do
      end if
      end do

      !check, whether the current position is a fixpoint or not
      compare = 0
      do v=1, 6
        if(fixfound(v)) then
          call adjustStep(stepsize(v), terminate, opt)
          if(.NOT. terminate) compare = 1
        end if
      end do
      if(compare /= 1) then
        terminate = .TRUE.
      end if

      print*, "current stepsize: ", stepsize

      qval(4000) = qval(4000) + 1
      print*, "ARRAYINDEX: ", qval(4000)

    end subroutine

    subroutine linmin(i,j,k,invparams, translation, wcoords, found, fixfound, stepsize, gradMat, opt, &
                      jac1, jac2, func1, func2, const, icheck, qval, gradarray, recalc)
      real, intent(in)                            :: const, icheck
      integer, intent(in)                         :: i,j,k
      real, dimension(:,:), intent(inout)         :: invparams, func1, func2, jac1, jac2
      real, dimension(:), intent(inout)           :: translation, qval
      type(tupel), dimension(:,:), intent(in)     :: wcoords
      logical, intent(inout)                      :: found, recalc
      real, dimension(:), intent(inout)           :: stepsize, gradarray
      real                                        :: grad, newopt, oldp, oldt,detJ
      integer                                     :: u,v,w,m
      real, dimension(1:2)                        :: point,ttranslation
      real, dimension(1:2,1:2)                    :: tparams,jac
      real, dimension(:,:), intent(inout)         :: gradMat
      real                                        :: intCheck
      logical, dimension(:), intent(in)           :: fixfound
      integer                                     :: linmincount
      real, intent(inout)                         :: opt

      linmincount = 0
      intCheck = 0.0
      tparams = invparams
      ttranslation = translation
      found = .FALSE.
      if(k==1) then
        do while(.NOT. found)
          print*, i,j,k
          !calculate gradMatrix
          if(recalc) then
            do u=1, size(wcoords(1,:))
              do v=1, size(wcoords(:,1))
                func2(u,v) = fw(point, invparams, translation)
                point = wcoords(u,v)%getCoords()
                !for each point
                do w=1, size(jac(1,:))
                  do m=1, size(jac(:,1))
                    jac(w,m) = JacobianComponents(w,m,invparams,translation,point)
                  end do
                end do
                detJ = jac(1,1)*jac(2,2)-jac(1,2)*jac(2,1)
                func1(u,v) = detJ
                gradMat(u,v) = gradientComponent(i,j,k, point, invparams, translation, &
                                                detJ, jac, func2(u,v), const)
              end do
            end do
            opt = optInt(wcoords, invparams, translation, const, qval(int(qval(4000))))
            if(i==1) then
              gradarray(j) = mcIntGrad(gradMat)
            else
              gradarray(j+2) = mcIntGrad(gradMat)
            end if
            !recalc = .FALSE.
          end if
          oldp = invparams(i,j)

          if(grad>0) then
            if(i==1) then
              tparams(i,j) = invparams(i,j) - stepsize(j)
            else
              tparams(i,j) = invparams(i,j) - stepsize(j+2)
            end if
          else
            if(i==1) then
              tparams(i,j) = invparams(i,j) + stepsize(j)
            else
              tparams(i,j) = invparams(i,j) + stepsize(j+2)
            end if
          end if
          !integrals with the new parameters
          if(i==1) then
            grad = gradarray(j)
          else
            grad = gradarray(j+2)
          end if

          call OptimizerRoutine(newopt, intcheck, wcoords, tparams, translation, const, func2(1,1))
          print*, "gradient: ", grad
          print*, "old Optimizer", opt
          print*, "new Optimizer", newopt
          print*, "contractionintegral", intCheck

          if(newopt<opt .AND. intcheck>icheck) then
            invparams = tparams
            linmincount = linmincount + 1
            recalc = .TRUE.
          end if

          if((opt<=0.001 .OR. invparams(i,j)==oldp)) then
            found = .TRUE.

            if(linmincount == 0) call fixpoint(fixfound,i,j,k)

            print*, " "
            print*, "terminate linmin"
            print*, " "
          end if
        end do
      else
        do while(.NOT. found)
          print*, i,j,k
          !calculate gradMatrix
          if(recalc) then
            do u=1, size(wcoords(1,:))
              do v=1, size(wcoords(:,1))
                point = wcoords(u,v)%getCoords()
                func2(u,v) = fw(point, invparams, translation)
                !for each point
                do w=1, size(jac(1,:))
                  do m=1, size(jac(:,1))
                    jac(w,m) = JacobianComponents(w,m,invparams,translation,point)
                  end do
                end do

                detJ = jac(1,1)*jac(2,2)-jac(1,2)*jac(2,1)
                func1(u,v) = detJ
                gradMat(u,v) = gradientComponent(i,j,k, point, invparams, translation, &
                                                detJ, jac, func2(u,v), const)
              end do
            end do
            opt = optInt(wcoords, invparams, translation, const, qval(int(qval(4000))))
            gradarray(i+4) = mcIntGrad(gradMat)
            print*, gradarray
            if(i==2) recalc = .FALSE.
          end if
          oldt = translation(i)
          if(grad>0) then
            ttranslation(i) = translation(i) - stepsize(4+i)
          else
            ttranslation(i) = translation(i) + stepsize(4+i)
          end if
          grad = gradarray(i+4)
          call OptimizerRoutine(newopt, intCheck, wcoords, invparams, ttranslation, const, func2(1,1))
          print*, "gradient: ", grad
          print*, "old Optimizer", opt
          print*, "new Optimizer", newopt
          print*, "contractionintegral", intCheck

          !if(newopt<opt .AND. intCheck>4.) translation = ttranslation
          if(newopt<opt .AND.intCheck>icheck) then
            linmincount = linmincount + 1
            translation = ttranslation
            recalc = .TRUE.
          end if

          if((opt<=0.001 .OR. translation(i)==oldt)) then
            found = .TRUE.

            if(linmincount == 0) call fixpoint(fixfound,i,j,k)

            print*, " "
            print*, "terminate linmin"
            print*, " "
          end if
        end do
      end if
    end subroutine

    subroutine fixpoint(fixfound,i,j,k)
      logical, dimension(:)        :: fixfound
      integer, intent(in)             :: i,j,k
      if(k==1) then
        if(i==1) then
          fixfound(j) = .TRUE.
        else
          fixfound(2+j) = .TRUE.
        end if
      else
        fixfound(4+i) = .TRUE.
      end if

    end subroutine

    subroutine adjustStep(stepwidth, terminate, optimizer)
      real, intent(inout)                           :: stepwidth
      logical, intent(inout)                        :: terminate
      real, intent(in)                              :: optimizer
      if(stepwidth>10.**(-4)) then
        if(optimizer>=1) then
          stepwidth = stepwidth/1.05
        else
          stepwidth = stepwidth/(1+optimizer**2)
        end if
        terminate = .FALSE.
      else
        terminate = .TRUE.
      end if

      print*, "new stepsize: ", stepwidth

    end subroutine

    subroutine fixpointDetection(detected, oldparams, newparams, oldtranslation, newtranslation)
      logical, intent(inout)                        :: detected
      real, dimension(:,:)                          :: oldparams, newparams
      real, dimension(:)                            :: oldtranslation, newtranslation
      integer                                       :: i,j

      do i=1, size(oldparams(1,:))
        if(oldtranslation(i)==newtranslation(i)) then
          detected = .TRUE.
        else
          detected = .FALSE.
        end if
        do j=1, size(oldparams(:,1))
          if(oldparams(i,j)==newparams(i,j)) then
            detected = .TRUE.
          else
            detected = .FALSE.
          end if
        end do
      end do

    end subroutine

    subroutine startTransformation(params, translation, mina, maxa, mint, maxt)
      real, dimension(:,:), intent(inout)           :: params
      real, dimension(:), intent(inout)             :: translation
      real, intent(in)                              :: mina, maxa, mint, maxt
      integer                                       :: i,j

      do i=1, size(params(1,:))
        translation(i) = randomPoint()*(maxt-mint)+mint
        do j=1, size(params(:,1))
          params(i,j) = randomPoint()*(maxa-mina)+mina
        end do
      end do


    end subroutine

end module
