program main


        use output
        use initialization
        use transform
        use calculus
        use algorithm
        !own classes
        use coordinates !provides a tupel class for coordinates

        implicit none

        !remember: real(kind=4) has 4 bytes
        real, allocatable, dimension(:)                      :: xcomp, y
        real, allocatable, dimension(:,:)                    :: func1, func2, gradMat
        type(tupel), allocatable, dimension(:,:)             :: coords, oldcoords
        real, dimension(1:2)                                 :: tempTupel, translation, comparetranslation
        real, dimension(1:4000)                               :: qray, erray
        real, dimension(1:4000)                             :: varray
        integer                                              :: unumber, io_error, uNrplot
        integer                                              :: reclen
        integer                                              :: pointDensity
        integer                                              :: i,counter
        real, dimension(2,2)                                 :: params, invparams, startparams, compareparams
        logical                                              :: run, terminate, repeat, op_test, op_int, &
                                                                op_plotq, op_plotv, op_ploterr, recalc
        real, dimension(1:2,1:2)                             :: oldp
        real, dimension(1:2)                                 :: oldt, starttranslation
        real                                                 :: a, intcheck, trash, var, optvalue
        real, dimension(1:6)                                 :: stepsize, gradarray
        real, dimension(1:100)                                :: densityarray
        real                                                 :: initial, finish, const, intMin, optMax, meanMin, &
                                                                meanMax, finalopt, finalquality, opt, intval
        integer, allocatable, dimension(:)                                :: seed
        integer                                               :: af, tnr, repitition, mainrun, startcount, qcount

        INTEGER*4 ::hostnm, status
        CHARACTER*32 :: name
        status = hostnm( name )
        name = '_'//adjustl(trim(name))

        !timestamp for computation time
        call cpu_time(initial)
        call system_clock(af)
        !initialize random_number generator
        call srand(af)

        intval = 21.914

        uNrplot = 31          !Unitnumber for var/q/errorplot
        repeat = .TRUE.       !logical for repeating the whole learning cycle
        run = .TRUE.          !used on several loops
        stepsize = 10.0       !start gridsize
        const = 1.            !not used rightnow
        counter = 10          !counter for some later loop
        repitition = 0        !repitition counter of the learning cycle

        meanMin = 0           !calculate the mean of all integrals of starting params
        meanMax = 0           !mean of all optimizers of starting params
        intMin = 4.           !used to get a starting lower bound
        optMax = 500          !used to get a starting upper bound
        startcount = 20       !not used


        !transformation start parameters
        params(1,1) =  1.0
        params(1,2) =  0.0
        params(2,1) =  0.0
        params(2,2) =  1.0
        !params = (/0.5, 0.5, 0.1, 0.2)/)
        translation = (/0.0, 0.0/)

        !get all data from the config.txt file
        call init_config(pointDensity, op_test, op_int, op_plotq, op_plotv, op_ploterr,&
                          params, translation)


        intCheck = 0  !reset intCheck
        if(.NOT. op_int) then
          do mainrun=1, 2     !loop 1: just fit the sample, loop 2: calculate integral with more points

            unumber = 15              !unitnumber for the scatterplot
            if(mainrun ==1) then
              pointDensity = 200      !sqrt of the number of points
            end if
            !x and y induces the point density of the grid
            allocate(xcomp(1:pointDensity))
            allocate(y(1:pointDensity))
            allocate(coords(size(xcomp),size(y)))
            allocate(func1(size(xcomp),size(y)))
            allocate(func2(size(xcomp),size(y)))
            allocate(gradMat(size(coords(1,:)),size(coords(:,1))))

            !create the actual coordinates
            print*, "creating point set"
            call createPoints(coords) !coords are here uniformly distributed
            oldcoords = coords      !safe for later usage

            !determine random start parameters:
            print*, "searching for start parameters"
            if(mainrun ==1) then
              run = .TRUE.
            else
              run = .FALSE.       !on second run, the params are fixed
            end if

            do while(run)         !until starting params are found
              startcount = 1
              !do while(run .AND. counter > 0)   !do 100 start params and calc the means
                print*, "."

                counter = 1
                do while(counter<100)
                  call startTransformation(params, translation, -15., 15., -5.1, 5.1) !range of param space
                  invparams = invertParams(params)        !OptimizerRoutine needs the inverse of params

                  !calc the optimizer(thrash), and the pure integral (intcheck), qray is just dumped here
                  call OptimizerRoutine(trash, intcheck, coords, invparams, translation, const, qray(1))
                  print*, intcheck, trash
                  meanMin = intcheck + meanMin
                  meanMax = trash + meanMax
                  counter = counter + 1
                  if(counter == 100) then     !calculate the means and set the min/max values
                    meanMin = meanMin/counter
                    meanMax = meanMax/counter
                  end if
                end do

                intMin = meanMin    !set min/max values
                optMax = meanMax
                qcount = 1          !counter for the quality-value array
                do while(run)
                  call startTransformation(params, translation, -10., 10., -1.1, 1.1)
                  invparams = invertParams(params)
                  !quality value is here safed in the q-array for later plots
                  call OptimizerRoutine(trash, intcheck, coords, invparams, translation, const, qray(qcount))
                  !if a parameterization is found that lives in the choosen space, fix it
                  if(intcheck>intMin .AND. trash < optMax) then
                    run = .FALSE.
                  end if
                  print*, intcheck, trash
                  qcount = qcount + 1
                end do
                qray(4000) = qcount     !safes the current arrayposition for later routines
                const = 1               !just for safety
                startcount = startcount +  1
              print*, "optMax = ", optMax, intMin
            end do

            !now start to get the best sample
            !do while(repeat)
                      !adjust the constant for smaller optimizer values
                      !optvalue = HUGE(optvalue)
                      !print*, "finding ideal optimizer constant"
                      !find the optimizerconstant
                      !do while(repeat)
                      !  call OptimizerRoutine(trash, intcheck, coords, invparams, translation, const, meanMin)!meanMin is just used as a dump here
                      !  const = const + 1.
                      !  if(const /= 1. .AND. trash<optvalue) then
                      !    optvalue = trash
                      !    print*, const
                      !  else
                      !    repeat = .FALSE.
                      !    print*, "optimal constant found", const
                      !  end if
                      !end do
                    !end do
                    !  trash = 0
                    !  intcheck = 0

                    !  repeat = .TRUE.

                    !  end do

            startparams = params                  !safe these for later print
            starttranslation = translation

            invparams=invertParams(params)        !invert for usage of integral routinges

            if(mainrun ==1) then
              repeat = .TRUE.
            else
              repeat = .FALSE.
            end if

            do while(repeat)
              repitition = repitition + 1
              print*, "repeat: "
              stepsize = 10.0                     !starting grid size

              run = .TRUE.
              !translation = (/5.1, -2.0/)

              terminate = .FALSE.                 !if true, the optimization will stop for the current cylce

              comparetranslation = translation    !compare later to find out if params are a fixpoint or not
              compareparams = invparams
              const = 1.
              recalc = .TRUE.
              do while(.NOT. terminate)

                oldt = translation
                oldp = params
                !optimize params for best importance sampling
                call optimize(coords, invparams, translation, stepsize, gradMat, terminate, func1, func2,&
                              const, intMin, qray, opt, gradarray, recalc)
                call fixpointDetection(run, oldp, invparams, oldt, translation)

              end do

              repeat = .FALSE.
              !check if the current point in param-space is a fixpoint or not
              do counter=1, 2
                if(translation(counter) /= comparetranslation(counter)) then
                  repeat = .TRUE.
                end if
                do i=1,2
                  if(compareparams(counter,i)/=invparams(counter,i)) then
                    repeat = .TRUE.
                  end if
                end do
              end do

            end do

            !------------------------------------CYCLE END

            !write data(if enabled) into some files
            if(op_plotq .OR. op_plotv .OR. op_ploterr) then

              if(op_plotq) then
                qray(4000) = 0    !just for a more beautiful plot
                !plot of quality value over the cycle
                open(unit=uNrplot, file='Plots/qualityplot'//name, iostat = io_error, status = 'replace')
                call writeArrayCLEAN(uNrplot, qray, io_error)
                close(unit = uNrplot, status = 'keep')
              end if
              if(op_ploterr) then
                !plot error to real integral value over the cycle
                call writeArray(uNrplot+2, erray, io_error)
                print*, "writeploterr"
              end if
            end if
            if(mainrun == 2) then
              !transform the params into the sampled space
              print*, "transforming coordinates"
              !transformation gives an error about 0.25 percentage of the integral a
              finalopt = optInt(coords, invparams, translation, const, finalquality)

              call backtrafo(coords, invparams, translation)

              !invert params back
              params = invertParams(invparams)
              print*, "calculating the final integral"
              !coords are now represented in the x-coordinates
              !integral value in the x-space
              a = mcInt(coords, params, (/1.0,1.0/), (/-1.0, -1.0/), translation, var)
              print*, "Integral value with transformed coordinates: ", a ,"+-", sqrt(var)
              print*, "deviation(%): ", abs(1-a/860.892)*100


              tempTupel = 0
              !get transformed coordinates
              do i=1, size(xcomp)
                tempTupel = coords(i,1)%getCoords()
                xcomp(i) = tempTupel(1)
                tempTupel = coords(1,i)%getCoords()
                y(i) = tempTupel(2)
              end do

              print*, "start Parameters:"
              call printParams(startparams, starttranslation)
              print*, "final parameters:"
              call printParams(params, translation)

              print*, "writing coordinates in file"
              !file to put just some coordinate scatter plot
              inquire(iolength = reclen) xcomp
              open(unit=unumber, file = 'Plots/dataScatterX'//name, access='direct', recl=reclen, iostat=io_error)
              call writeArray(unumber, xcomp, io_error)
              close(unit = unumber, status = 'keep')
              inquire(iolength = reclen) y
              open(unit=unumber, file = 'Plots/dataScatterY'//name, access='direct', recl=reclen, iostat=io_error)
              call writeArray(unumber, y, io_error)
              close(unit = unumber, status = 'keep')
            end if

            !set free the memory for x, y and func
            deallocate(xcomp)
            deallocate(y)
            deallocate(func1)
            deallocate(func2)
            deallocate(coords)
            deallocate(gradmat)
          end do

          !just do the integral with a given parameters
        else
          call init_config(pointDensity, op_test, op_int, op_plotq, op_plotv, op_ploterr, &
                          params, translation)

          allocate(xcomp(1:pointDensity))
          allocate(y(1:pointDensity))
          allocate(coords(size(xcomp),size(y)))
          allocate(func1(size(xcomp),size(y)))
          allocate(func2(size(xcomp),size(y)))
          allocate(gradMat(size(coords(1,:)),size(coords(:,1))))

          !create the actual coordinates
          print*, "creating point set"
          call createPoints(coords)
          oldcoords = coords

          invparams=invertParams(params)

          print*, "transforming coordinates"
          !transformation gives an error about 0.25 percentage of the integral a
          call backtrafo(coords, invparams, translation)

          tempTupel = 0
          !get transformed coordinates
          do i=1, size(xcomp)
            tempTupel = coords(i,1)%getCoords()
            xcomp(i) = tempTupel(1)
            tempTupel = coords(1,i)%getCoords()
            y(i) = tempTupel(2)
          end do

          print*, "writing coordinates in file"
          !file to put just some coordinate scatter plot
          inquire(iolength = reclen) xcomp
          open(unit=unumber, file = 'Plots/dataScatterX', access='direct', recl=reclen, iostat=io_error)
          call writeArray(unumber, xcomp, io_error)
          close(unit = unumber, status = 'keep')
          inquire(iolength = reclen) y
          open(unit=unumber, file = 'Plots/dataScatterY', access='direct', recl=reclen, iostat=io_error)
          call writeArray(unumber, y, io_error)
          close(unit = unumber, status = 'keep')

          print*, "calculating the final integral"
          !coords are now represented in the x-coordinates
          a = mcInt(coords, params, (/1.0,1.0/), (/-1.0, -1.0/), translation, var)
          print*, "Integral value with transformed coordinates: ", a ,"+-", sqrt(var)
          print*, "deviation(%): ", abs(1-a/860.892)*100

          !a = density(oldcoords, params, translation)
          !print*, "integral over density matrix: ", a

          deallocate(xcomp)
          deallocate(y)
          deallocate(func1)
          deallocate(func2)
          deallocate(coords)
          deallocate(gradmat)

        end if
        !plot variance over different number of points
        if(op_plotv .OR. op_ploterr) then
          call init_config(pointDensity, op_test, op_int, op_plotq, op_plotv, op_ploterr, &
                          params, translation)
                          varray = 0.
          do pointDensity=1, 1000

            allocate(xcomp(1:pointDensity))
            allocate(y(1:pointDensity))
            allocate(coords(size(xcomp),size(y)))
            allocate(func1(size(xcomp),size(y)))
            allocate(func2(size(xcomp),size(y)))
            allocate(gradMat(size(coords(1,:)),size(coords(:,1))))

            call createPoints(coords)
            oldcoords = coords

            print*, "transforming coordinates"
            invparams = invertParams(params)
            !transformation gives an error about 0.25 percentage of the integral a
            call backtrafo(coords, invparams, translation)

            print*, "calculating the final integral"
            print*, pointDensity
            !coords are now represented in the x-coordinates
            a = mcInt(coords, params, (/1.0,1.0/), (/-1.0, -1.0/), translation, var)
            varray(pointDensity) = sqrt(abs(var))
            erray(pointDensity) = abs((intval-a)/intval)

            deallocate(xcomp)
            deallocate(y)
            deallocate(func1)
            deallocate(func2)
            deallocate(coords)
            deallocate(gradmat)
          end do

          if(op_ploterr) then
            open(uNrplot+1, file='Plots/err'//name, iostat = io_error, status = 'replace')
            call writeArrayCLEAN(uNrplot+1, erray, io_error)
            close(unit = uNrplot+1, status='keep')
          else
            open(uNrplot+1, file='Plots/varplot'//name, iostat = io_error, status = 'replace')
            call writeArrayCLEAN(uNrplot+1, varray, io_error)
            close(unit = uNrplot+1, status='keep')
          end if
        end if

        call cpu_time(finish)
        print*, "programm executed in : ", (finish-initial), "seconds"
        print*, "repeated: ", repitition

        open(177, file='Plots/data'//name, iostat = io_error, status = 'replace')
        call writeData(177, io_error, params, translation, finish-initial, pointDensity, a, var, finalopt, finalquality)
        close(unit = 177, status = 'keep')
      contains


end program
