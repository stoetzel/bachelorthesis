#some little script to plott a two-dimensional function

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
import array

#read in the data to plot
filename1 = "data"
filename2 = "datatrafo"
points = []#use append to add several rows
file = open(filename1, 'rb')
bytes = bytearray(file.read())

points = array.array('f', bytes)

file.close()

npPoints = np.array(points)
npPoints = npPoints.reshape((int(np.sqrt(npPoints.shape[0])), int(np.sqrt(npPoints.shape[0]))))

#Coordinate axis points
x = np.arange(-1,1,2/(len(npPoints)))
y = np.arange(-1,1,2/(len(npPoints)))

#create meshgrid
x,y = np.meshgrid(x,y)

#function to plot
#f = np.sin(np.sqrt(x**2 + y**2))
#fig = plt.figure()             #doesnt do anything
ax = plt.gca(projection='3d') #dunno, 3d plot enable and something else
ax2 = plt.gca(projection='3d')

#print(x.shape)
#print(y.shape)

#plot surface
surf = ax.plot_wireframe(x, y, npPoints, rstride=1, cstride=1, color='r')#cmap=cm.coolwarm, linewidth=0, antialiased=False)

file = open(filename2, 'rb')
bytes = bytearray(file.read())

points = array.array('f', bytes)

file.close()
npPoints = np.array(points)
npPoints = npPoints.reshape((int(np.sqrt(npPoints.shape[0])), int(np.sqrt(npPoints.shape[0]))))

surf2 = ax2.plot_wireframe(x, y, npPoints, rstride=1, cstride=1, color='g')

#Customize the z axis
ax.set_zlim(-1.01, 1.01)
ax.zaxis.set_major_locator(LinearLocator(10)) #numbers of horizontal lines at the z-axis
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f')) #number of integers per number

#Customize the z axis
ax2.set_zlim(-1.01, 1.01)
ax2.zaxis.set_major_locator(LinearLocator(10)) #numbers of horizontal lines at the z-axis
ax2.zaxis.set_major_formatter(FormatStrFormatter('%.02f')) #number of integers per number


#Add color bar which maps values to colors
#fig.colorbar(surf, shrink=0.5, aspect=5)

#Show the plot
plt.show()
