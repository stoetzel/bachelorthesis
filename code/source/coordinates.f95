module coordinates
  implicit none

  !to be expanded to n dimensional tupel
  
  !integer, parameter :: p = selected_real_kind(30,256)
  !real(kind=p)
  type :: tupel
    !integer                         :: dim
    real, dimension(1:2) :: tup      !default werte setzen!

  contains
    !only has set and get method yet
    procedure :: getCoords => getCoordinates
    procedure :: getX => getXCoord
    procedure :: getY => getYCoord
    procedure :: setk => setkCoord
    procedure :: getk => getkCoord
    procedure :: setCoords => setCoordinates
    procedure :: empty => zeroCoordinates
  end type

  contains
    !get tupel elements, output is array
    function getCoordinates(this)
      class(tupel) :: this
      real, dimension(1:2) :: getCoordinates
      getCoordinates = this%tup
    end function

    !set tupel elements, input is an array
    subroutine setCoordinates(this, newC)
      class(tupel)                                :: this
      real, dimension(1:2), intent(in)            :: newC
      this%tup = newC
    end subroutine

    function getXCoord(this)
      class(tupel)  :: this
      real                  :: getXCoord
      getXCoord = this%tup(1)
    end function

    subroutine setkCoord(this, x, k)
      class(tupel)  :: this
      real, intent(in)  :: x
      integer, intent(in) :: k
      this%tup(k) = x
    end subroutine

    real function getkCoord(this, k)
      class(tupel)         :: this
      integer, intent(in) :: k
      getkCoord = this%tup(k)
    end function

    function getYCoord(this)
      class(tupel) :: this
      real          :: getYCoord
      getYCoord = this%tup(2)
    end function

    subroutine zeroCoordinates(this)
      class(tupel) :: this
      call this%setCoords((/0.0,0.0/))
    end subroutine

end module coordinates
