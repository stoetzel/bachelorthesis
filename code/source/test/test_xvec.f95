program test_xvec

  use calculus

  implicit none

  real, dimension(1:2)        :: point, translation, testX, calcX
  real, dimension(1:2,1:2)    :: params
  integer                     :: i

  point = (/0.5, -0.2/)
  translation = (/1.0,-0.5/)
  params(1,1) = 1.5
  params(1,2) = -0.2
  params(2,1) = 0.3
  params(2,2) = 2.0

  testX = (/-0.269, 0.188/)

  do i=1, 2
    calcX(i) = xvec(params, translation, point, i)
  end do

  do i=1, 2
    if(abs(calcX(i)-testX(i))>0.01) then
      print*, "no match! ", calcX(i), testX(i)
      stop 43
    else
      stop 0
    end if
  end do

end program
