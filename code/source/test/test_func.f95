program test_func

  use calculus

  implicit none

  real, dimension(1:2)        :: point, translation
  real, dimension(1:2,1:2)    :: params
  real                        :: testValue, calcValue

  point = (/0.5, -0.2/)
  translation = (/1.0,-0.5/)
  params(1,1) = 1.5
  params(1,2) = -0.2
  params(2,1) = 0.3
  params(2,2) = 2.0

  testValue = 0.196

  calcValue = fw(point, params, translation)

  if(abs(calcValue-testValue)>0.01) then
    print*, "no match! ", calcValue, testValue
    stop 43
  else
    stop 0
  end if


end program
