program test_jacobian

  use calculus

  implicit none
  !testfunction is: 3(x+y)^3+2x-2y

  real, dimension(1:2,1:2)    :: params
  real, dimension(1:2)        :: translation, Point
  real, dimension(1:2,1:2)    :: testjac, jac
  integer                     :: i,j

  point = (/0.5, -0.2/)
  params(1,1) =   1.5
  params(1,2) =  -0.2
  params(2,1) =   0.3
  params(2,2) =   2.0
  translation = (/1.0, -0.5/)
  jac = 0.0

  testjac(1,1) =  1.213
  testjac(1,2) =  -0.155
  testjac(2,1) =  0.310
  testjac(2,2) =  1.990

  do i=1, 2
    do j=1, 2
      jac(i,j) = JacobianComponents(i,j, params, translation, point)
      if (abs(testjac(i,j) - jac(i,j))>0.005) then
        print*, "missmatch in :", testjac(i,j), " and ", jac(i,j), i, j
        stop 43
      end if
    end do
  end do

end program
