program test_determinant

  use calculus

  implicit none

  real, dimension(1:3,1:3)    :: matrix, umat, lmat, pmat
  real                        :: detval, testvalue
  integer                     :: nop

  matrix(1,1) = 1.0
  matrix(1,2) = 2.0
  matrix(1,3) = 0.0
  matrix(2,1) = 1.0
  matrix(2,2) = 0.0
  matrix(2,3) = 3.0
  matrix(3,1) = -2.0
  matrix(3,2) = 2.0
  matrix(3,3) = 1.0

  call decomp(matrix, lmat, umat, 3, nop, pmat)

  detval = determinantLU(umat, lmat)
  testvalue = -20.0

  if (abs(detval- testvalue)>0.0) then
    print*, "missmatch in :", detval, " and ", testvalue
    stop 43
  end if

end program
