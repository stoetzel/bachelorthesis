program test_gradient

  use calculus
  use coordinates
  use transform
  use LUdecomp

  implicit none
  !testfunction is: 3*x^2+(x-2)*y
  type(tupel), dimension(1:1,1:1) :: coords
  real, dimension(1:2,1:2)    :: params, jacobian, gmat, lmatJ, umatJ, tempjac, pmat, invparams
  real, dimension(1:2)        :: point, trafopoint, temp, translation
  real, dimension(1:6)          :: gradient, testgradient
  real                        :: delta, detJ
  integer                     :: i,j,l,r,n, nop
  logical                     :: test

  test = .TRUE.

  i = 1
  j = 1
  l = 0
  params(1,1) =  0.3
  params(1,2) =  -1.3
  params(2,1) = 0.7
  params(2,2) =  2.1
  translation = (/0.5, -0.3/)
  delta = 0.001

    point = (/0.5, -0.2/)

    print*, "params: ", params
    print*, "invparams: ", invertParams(params)
    print*, "xvec1", xvec(params, translation, point, 1)

    print*, "xvec2", xvec(params, translation, point, 2)

    print*, "functionvalue: ", fw(point, params, translation, test)

    print*, "jacobian", jacobian

  do r=1, 2
    do n=1, 2
      jacobian(r,n) = JacobianComponents(r,n,params, translation, point)
    end do
  end do

  print*, "jacobian", jacobian

  testgradient(1) =  -1.307
  testgradient(2) =  -0.3976
  testgradient(3) =  0.649
  testgradient(4) =  -0.170
  testgradient(5) =  -0.693
  testgradient(6) =  -0.6417

  print*, jacobian

  detJ = jacobian(1,1)*jacobian(2,2)-jacobian(1,2)*jacobian(2,1)
  print*, detJ

  print*, "derivative 1: ", analyticDerivative(point, params, translation, 1, test)
  print*, "derivative 2: ", analyticDerivative(point, params, translation, 2, test)

  invparams = invertParams(params)

  n=1
  do l=1,2
    do i=1, 2
      j=1
      do while(j<=2 .AND. l/=2)
        gradient(n) = gradientComponent(i,j,l, point, params, translation, &
                                  detJ, test)
        if (abs(testgradient(n) - gradient(n))>0.005) then
          print*, "missmatch in :", testgradient(n), " and ", gradient(n),i,j,l!, i
          stop 43
        end if
        n = n+1
        j = j+1
      end do
      if(l==2) then
        gradient(n) = gradientComponent(i,j,l, point, params, translation, &
                                  detJ, test)
        if (abs(testgradient(n) - gradient(n))>0.005) then
          print*, "missmatch in :", testgradient(n), " and ", gradient(n),i,j,l!, i
          stop 43
        end if
        n = n+1
      end if
    end do
  end do

end program
