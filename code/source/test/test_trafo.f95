program test_trafo

      use transform
      use coordinates
      implicit none

      type(tupel), dimension(1:2,1:2)         :: coords, testCoords
      real, dimension(2,2)                    :: g
      real, dimension(1:2)                    :: t,s, translation
      real, dimension(1:4)                    :: params
      integer                                 :: i, j

      params = (/1.0, -3.0, 2.0, 1.0/)
      translation = (/1.5, -1.0/)
      g = 0
      !coords = reshape((/[-2.3,-2.3], [-2.3, 1.7], [1.7, -2.3], [1.7,1.7]/), shape(coords), order=(/1,2/))
      call coords(1,1)%setCoords([-0.99,-0.5])
      call coords(1,2)%setCoords([-0.95, 0.97])
      call coords(2,1)%setCoords([0.8, -0.5])
      call coords(2,2)%setCoords([0.5, 0.99])

      call testCoords(1,1)%setCoords([0.4631, -0.9999])
      call testCoords(1,2)%setCoords([0.9999, -0.9883])
      call testCoords(2,1)%setCoords([0.9674, 0.5703])
      call testCoords(2,2)%setCoords([-0.9999, 0.9918])

      call trafo(coords, params, translation)

      do i=1, 2
        do j=1, 2
          t = testCoords(i,j)%getCoords()
          s = coords(i,j)%getCoords()
          print*, s
          print*, abs(s(1)-t(1))
          if (abs(s(1)-t(1))>0.01 .OR. abs(t(2)-s(2))>0.01) then
            print*, "x'-x:", t(1), "y'-y:",  t(2), "at(i,j):", i, j
            stop 43
          end if
        end do
      end do
      stop 0

end program
