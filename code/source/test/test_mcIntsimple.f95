program test_mcIntsimple

  use calculus
  use coordinates
  use transform
  use initialization
  use output

  implicit none
  !testfunction is: 3*x^2+(x-2)*y
  type(tupel), dimension(1000,1000)                   :: coords
  real                                                :: int, testint

  testint = 4.0

  call createPoints(coords)

  int = mcInt(coords)

    if (abs(testint - int)>0.005) then
      print*, "missmatch in :", testint, " and ", int!, i
      stop 43
    end if

end program
