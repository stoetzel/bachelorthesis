program test_deriv

  use calculus

  implicit none
  !testfunction is: 3(x+y)^3+2x-2y

  real, dimension(1:2,1:2) :: params
  real, dimension(1:2)        :: testPoint, Point, translation, temp
  integer                     :: i
  logical                     :: test

  test = .TRUE.


  params(1,1) = 0.3
  params(1,2) = -1.3
  params(2,1) = 0.7
  params(2,2) = 10.1
  translation = (/0.5, -0.3/)

  point=(/0.5, -0.2/)

  testPoint = (/3.567, -0.2/)

  temp(1) = analyticDerivative(point, params, translation, 1, test)
  temp(2) = analyticDerivative(point, params, translation, 2, test)

  do i=1, 2
    if (abs(testPoint(i) - temp(i))>0.1) then
      print*, "missmatch in :", testPoint(i), " and ", temp(i), i
      stop 43
    end if
  end do

end program
