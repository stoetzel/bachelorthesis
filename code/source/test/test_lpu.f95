program test_lpu

  use LUdecomp

  implicit none

  !real, dimension(2,2)    :: g
  !real, dimension(1:9)  ::   a = (/ 1, 1, 1, 2, 1, 3, 3, 1, 1 /)
  !real, dimension(1:3,1:3)  :: g
  real, dimension(1:4,1:4)  :: g, l, u, test, g_0, P
  integer               :: i,j,s


  l = 0
  u = 0

  s = 0
  g_0 = reshape((/ 0, 4, 2, 0, 2, 1, 2, 0, 1, 0, 1, 0, 0, 0, 0, 1/), shape(g_0), (/3,3/))
  g = reshape((/ 0, 4, 2, 0, 2, 1, 2, 0, 1, 0, 1, 0, 0, 0, 0, 1 /), shape(g), (/3,3/))
  P = 0

  !call permute(g, 4, 1, s)
  !print*, g

  !insert new l_0, u_0 for the permutet g!
  !l_0 = reshape((/1.0, 1.0, 3.0, 0.0, 1.0, 3.0, 0.0, 0.0, 1.0/),shape(l_0), (/3,3/))
  !l_0 = reshape((/1, 0, 3, 2, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1/),shape(l_0), (/3,3/))
  !l_0(4,3) = 1.5

  !u_0 = reshape((/1, 0, 0, 0, 1, 1, 0, 0, 2, 1, -2, 0, 1, 2, 0, -2/),shape(u_0),(/3,3/))

  print*, "matrix g before decomp: "
  print*, g
  call decomp(g, l, u, size(g(1,:)), s, P)
  print*, "matrix g: "
  print*, g
  print*, "matrix L: "
  print*, l
  print*, "matrix U: "
  print*, u

  print*, "matmul gives: "
  print*, P
  print*, matmul(u,l)
  print*, transpose(P)
  test = matmul(transpose(P),transpose(matmul(u,l)))    !without transpose the matrix gets messed up
  print*, test                                          !maybe some problem in indexing with reshape

  do i=1, 3
    do j=1, 3
      if ( test(i,j)-g_0(i,j) /= 0) then
        print*, "difference in A: ", test(i,j) ,"for i,j: ", i, j
        stop 43
      end if
    end do
  end do

  stop 0

end program test_lpu
