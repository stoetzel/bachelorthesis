program test_sort

  use initialization

  implicit none

  real, dimension(1:6)  :: testarray, compare
  integer               :: i

  testarray = (/4.2, -1.2, 2.3, -1.1, 0.0, 100.0/)
  compare = (/-1.2, -1.1, 0.0, 2.3, 4.2, 100.0/)

  call sortArray(testarray)


  do i=1, size(testarray)
    if (testarray(i) /= compare(i)) then
      print*, "missmatch in :", testarray(i), " and ", compare(i)
      stop 43
    end if
  end do

end program
